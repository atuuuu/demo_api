les fichiers start_go.bat et go_run.sh téléchargent automatiquement les dépences nécessaires au projet depuis github
Git doit être installé
Une base de donnée MySQL 5.7.31 doit être instalée et configurée comme suit :

Installez MySQL 5.7.31, par exemple à travers wampserver 3.2.3
Créez une base nommée 'samdoc' (sans les '), merci de bien respecter le nom pour que l'API fonctionne
Choisissez l'encodage UTF-8
Ensuite :

Depuis PhpMyAdmin :
    sur la nouvelle base samdoc:
        dans l'onglet "Importer":
            dans la catégorie "Fichier à importer" :
                Selectionnez le fichier "dumpfile.sql" contenu dans src/database
                Le jeu de caractère du fichier est en UTF-8
                Tout en bas à droite de la page, selectionnez "Executer"

Les identifiants par défaut de la base sont :
    id : demo_api
    password : AP1_pAS5

stats API documentation :
https://app.swaggerhub.com/apis/atuuuu/Samdoc_serveur_stats/1.0.0

patients API documentation :
https://app.swaggerhub.com/apis/atuuuu/Samdoc_serveur_patient/1.0.0
