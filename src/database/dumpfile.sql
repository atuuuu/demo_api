-- MySQL dump 10.13  Distrib 5.7.31, for Win64 (x86_64)
--
-- Host: localhost    Database: samdoc
-- ------------------------------------------------------
-- Server version	5.7.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+01:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `consulter`
--

DROP TABLE IF EXISTS `consulter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consulter` (
  `dateConsultation` date NOT NULL,
  `idPrescription` int(11) NOT NULL,
  `numSecu` int(11) NOT NULL,
  PRIMARY KEY (`dateConsultation`,`idPrescription`),
  KEY `cons_patient_fk` (`numSecu`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consulter`
--

LOCK TABLES `consulter` WRITE;
/*!40000 ALTER TABLE `consulter` DISABLE KEYS */;
INSERT INTO `consulter` VALUES ('2021-01-13',1,4);
/*!40000 ALTER TABLE `consulter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contaminer`
--

DROP TABLE IF EXISTS `contaminer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contaminer` (
  `symptomes` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `numSecu` int(11) NOT NULL,
  `idMaladie` int(11) NOT NULL,
  `dateContamination` date DEFAULT NULL,
  `idContamination` int(11) NOT NULL,
  PRIMARY KEY (`idContamination`),
  KEY `numSecu` (`numSecu`),
  KEY `mala_conta_fk` (`idMaladie`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contaminer`
--

LOCK TABLES `contaminer` WRITE;
/*!40000 ALTER TABLE `contaminer` DISABLE KEYS */;
INSERT INTO `contaminer` VALUES ('toux, fièvre',4,1,NULL,0),('Fatigue',4,2,NULL,1),('maux de crane',2,3,'2021-01-14',3),('maux de crane',5,3,'2021-01-14',4);
/*!40000 ALTER TABLE `contaminer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maladie`
--

DROP TABLE IF EXISTS `maladie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maladie` (
  `idMaladie` int(11) NOT NULL AUTO_INCREMENT,
  `nomMaladie` varchar(40) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idMaladie`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maladie`
--

LOCK TABLES `maladie` WRITE;
/*!40000 ALTER TABLE `maladie` DISABLE KEYS */;
INSERT INTO `maladie` VALUES (1,'covid-19'),(2,'insomnie'),(3,'grippe');
/*!40000 ALTER TABLE `maladie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicament`
--

DROP TABLE IF EXISTS `medicament`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicament` (
  `idMed` int(11) NOT NULL AUTO_INCREMENT,
  `nomMed` varchar(200) COLLATE utf8_bin NOT NULL,
  `descriptionMed` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `prixMed` int(11) NOT NULL,
  PRIMARY KEY (`idMed`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicament`
--

LOCK TABLES `medicament` WRITE;
/*!40000 ALTER TABLE `medicament` DISABLE KEYS */;
INSERT INTO `medicament` VALUES (1,'Dermophil indien','antidouleur',5),(2,'Doliprane','antidouleur',8),(3,'Smecta','pansement digestif',10);
/*!40000 ALTER TABLE `medicament` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `numSecu` int(11) NOT NULL DEFAULT '-1',
  `nom` varchar(120) COLLATE utf8_bin NOT NULL DEFAULT 'Non renseigné',
  `prenom` varchar(100) COLLATE utf8_bin DEFAULT 'Non renseigné',
  `dateNaissance` date DEFAULT '2001-01-01',
  `sexe` varchar(50) COLLATE utf8_bin DEFAULT '?',
  PRIMARY KEY (`numSecu`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Contient les différents patients';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (2,'DUPONT','Jean','2001-06-23','M'),(4,'ORWELL','George','1984-01-01','H'),(5,'CAMU','Albert','1913-11-07','H'),(6,'Jean','Jacque','1999-01-02','M'),(15,'Jean','Jacque','1999-01-02','H'),(1,'ANDERSON','Alexander','1980-01-02','H'),(3,'LAGAFFE','Gaston','1957-02-28','H');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prescription`
--

DROP TABLE IF EXISTS `prescription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prescription` (
  `idPrescription` int(11) NOT NULL,
  `idMed` int(11) NOT NULL,
  `freq` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `duree` int(11) DEFAULT NULL,
  PRIMARY KEY (`idMed`,`idPrescription`),
  KEY `cont_cons_fk` (`idPrescription`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prescription`
--

LOCK TABLES `prescription` WRITE;
/*!40000 ALTER TABLE `prescription` DISABLE KEYS */;
INSERT INTO `prescription` VALUES (1,2,'1 fois par jour',5),(2,1,'2 fois par jour',5);
/*!40000 ALTER TABLE `prescription` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-15 17:30:26
