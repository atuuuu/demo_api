package main

import (
	"database/sql"
	"fmt"
	"sync"
)

func main() {

	var wg = sync.WaitGroup{}
	defer wg.Done()

	//var conn, err = sql.Open("oracle", "oracle://SYSTEM:SYSTEM@localhost/xe")
	//var conn, err = sql.Open("oracle", "oracle://ETU2_28:ETU2_28@kiutoracle18.unicaen.fr/info?SID=info")
	var conn, err = sql.Open("mysql", "demo_api:AP1_pAS5@tcp(127.0.0.1:3306)/samdoc")
	CheckErr(err, nil)
	defer conn.Close()

	sPatients := ServeurPatients{Conn: conn}
	sStat := ServerStats{Conn: conn}

	wg.Add(1)
	fmt.Println("Lancement de la base stats...")
	go sStat.Run()

	wg.Add(1)
	fmt.Println("Lancement de la base médecin...")
	go sPatients.Run()

	fmt.Println("Lancement réussi !")
	wg.Wait()
}
