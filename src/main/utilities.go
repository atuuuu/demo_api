package main

import (
	"database/sql"
	"encoding/xml"
	"fmt"
	"net/http"
	"strings"
)

type patient struct {
	XMLName       xml.Name `xml:"patient"`
	NumSecu       int      `xml:"numSecu"`
	Nom           string   `xml:"nom"`
	Prenom        string   `xml:"prenom"`
	DateNaissance string   `xml:"dateNaissance"`
	Sexe          string   `xml:"sexe"`
}

func CheckErr(err error, w http.ResponseWriter) bool {
	if err != nil {
		if w != nil {
			_, _ = fmt.Fprint(w, err)
		} else {
			fmt.Println(err)
		}
		return false
	} else {
		return true
	}
}

////////////////TRAITEMENT DU XML//////////////////

func ReqToXml(w http.ResponseWriter, rows *sql.Rows, modele []string) string {
	var str strings.Builder

	var colonnes = make([]interface{}, len(modele))

	var values = make([]string, len(modele))

	for i := 0; i < len(modele); i++ {
		colonnes[i] = &values[i]
	}

	var err = rows.Scan(colonnes...)
	CheckErr(err, nil)

	for i := 0; i < len(values); i++ {
		str.WriteString("\t\t<" + modele[i] + ">")
		str.WriteString(values[i])
		str.WriteString("</" + modele[i] + ">\n")
	}

	return str.String()
}

func RetrivePatientFromRequestPost(xmlPatient string) patient {
	var pat patient
	strings.Replace(xmlPatient, "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n", "", 1)

	err := xml.Unmarshal([]byte(xmlPatient), &pat)
	if err != nil {
		CheckErr(err, nil)
	}
	return pat
}
