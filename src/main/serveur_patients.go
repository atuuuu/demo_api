package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
)

type ServeurPatients struct {
	routeur *mux.Router
	Conn    *sql.DB
}

///////////////////////////////////Init///////////////////////////////////

func (servP *ServeurPatients) Run() {
	servP.routeur = mux.NewRouter().StrictSlash(true) //Réponds aux requètes et sert les données
	servP.handleRequests()                            //Fonction mettant en place les handlers
}

func (servP *ServeurPatients) handleRequests() {
	servP.routeur.HandleFunc("/", servP.homePage)                                     //Page principale :
	servP.routeur.HandleFunc("/patients", servP.infoPatients)                         //Liste de tous les patients
	servP.routeur.HandleFunc("/patients/add", servP.createNewPatient).Methods("POST") //Ajout d'un patient (en xml)
	servP.routeur.HandleFunc("/patients/{numSecu}", servP.infoPatientsSpecifique)     //Information sur un seul patient
	log.Fatal(http.ListenAndServe(":2500", servP.routeur))                            //HTTP
	//log.Fatal(http.ListenAndServeTLS(":2500","localhost.crt", "localhost.key", servP.routeur))	//HTTPS -> requiert des certificats SSH reconnu (marche mais le navigateur affiche un message)
}

////////////////////////////////Home Page/////////////////////////////////

func (servP *ServeurPatients) homePage(w http.ResponseWriter, r *http.Request) {
	servP.displayHomePage(w)
}

func (servP *ServeurPatients) displayHomePage(w http.ResponseWriter) {
	fmt.Fprintf(w, "<h1>Page des patients :</h1> <br/>")
	fmt.Fprintf(w, "<div> <a href=www.localhost:2500/patients> Liste de patients </a> <br/>")
	fmt.Fprintf(w, "<a href=www.localhost:2500/patients/1> Info sur un seul patient </a> <br/>")
	fmt.Fprintf(w, "<a href=www.localhost:2500/patients:add> Lien pour la requète post pour l'ajout d'un patient </a> </div> <br/>")
}

//Au dela : code pour Create et Read concernant les patients

//////////////////////////////////////Handlers Read///////////////////////////////////////

//Liste de tous les patients
func (servP *ServeurPatients) infoPatients(w http.ResponseWriter, r *http.Request) {

	//<?xml version="1.0" encoding="UTF-8" ?>
	//<patients>
	//	<patient>
	//		<numSecu> 2 </numSecu>
	//		<nom> DUPONT </nom>
	//		<prenom> Jean </prenom>
	//		<dateNaissance> 23/06/2001 </dateNaissance>
	//		<sexe> M </sexe>
	//	</patient>
	//	...
	//</patients>

	//////////////////////////////REQUETE///////////////////////////////

	fmt.Println("envoi de la liste de tous les patients")

	stmt, err := servP.Conn.Prepare("SELECT numSecu, nom, prenom, dateNaissance, sexe FROM PATIENT")
	CheckErr(err, w)
	defer stmt.Close()

	var rows, err2 = stmt.Query()
	CheckErr(err2, w)
	defer rows.Close()

	//////////ECRITURE DU FICHIER XML//////////
	var str []string
	str, err = rows.Columns()
	CheckErr(err, w)

	servP.printPatientXML(w, rows, str)
}

func (servP *ServeurPatients) printPatientXML(w http.ResponseWriter, rows *sql.Rows, str []string) {
	fmt.Fprint(w, "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n")
	fmt.Fprint(w, "<patients>\n")

	for rows.Next() {
		fmt.Fprint(w, "\t<patient>\n")
		fmt.Fprint(w, ReqToXml(w, rows, str))
		fmt.Fprint(w, "\t</patient>\n")
	}

	fmt.Fprint(w, "</patients>")
}

//Information sur un seul patient (correspondant au numéro de sécurité passé dans le lien
func (servP *ServeurPatients) infoPatientsSpecifique(w http.ResponseWriter, r *http.Request) {

	p := mux.Vars(r)
	numSecu := p["numSecu"]

	modele := []string{"nom", "prenom", "nomMaladie", "date", "nomMed"}

	row, err := servP.Conn.Query("SELECT nom, prenom, nomMaladie, dateContamination, nomMed from patient LEFT JOIN contaminer USING (numSecu) LEFT JOIN maladie USING (idMaladie) LEFT JOIN consulter USING (numSecu) LEFT JOIN prescription USING (idPrescription) LEFT JOIN medicament USING (idMed) WHERE numSecu = ?", numSecu)
	CheckErr(err, w)
	defer row.Close()

	servP.writeInfoPatient(w, row, modele)
}

func (servP *ServeurPatients) writeInfoPatient(w http.ResponseWriter, row *sql.Rows, modele []string) {

	if !row.Next() {
		w.WriteHeader(http.StatusNoContent)
		fmt.Fprint(w, "<p>Pas de données...</p>")
	} else {
		fmt.Fprint(w, "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n")
		fmt.Fprint(w, "<patients>\n")

		//Affiche la première ligne pour ne pas perdre le premier row.Next() lancé dans le if
		fmt.Fprint(w, "\t<patient>\n")
		fmt.Fprint(w, ReqToXml(w, row, modele))
		fmt.Fprint(w, "\t</patient>\n")

		//Affiche les lignes suivantes
		for row.Next() {
			fmt.Fprint(w, "\t<patient>\n")
			fmt.Fprint(w, ReqToXml(w, row, modele))
			fmt.Fprint(w, "\t</patient>\n")
		}

		fmt.Fprint(w, "</patients>")
	}
}

/////////////////////////////HANDLER CREATE///////////////////////////

func (servP *ServeurPatients) createNewPatient(w http.ResponseWriter, r *http.Request) {
	req, err := ioutil.ReadAll(r.Body)
	CheckErr(err, w)

	////////////////RECUPERATION DES DONNEES DU PATIENT DEPUIS LA REQUETE/////////////////
	patient := RetrivePatientFromRequestPost(string(req))

	////////////////ENREGISTREMENT DANS LA BDD/////////////////////
	res, err := servP.Conn.Exec("INSERT INTO patient VALUES (?, ?, ?, str_to_date(?, '%d/%m/%Y'), ?)", patient.NumSecu, patient.Nom, patient.Prenom, patient.DateNaissance, patient.Sexe)
	if err == nil {
		w.WriteHeader(http.StatusCreated)
		fmt.Fprint(w, res)
	} else {
		w.WriteHeader(http.StatusNotAcceptable)
	}
	CheckErr(err, w)
}
