package main

import (
	"database/sql"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

type ServerStats struct {
	routeur *mux.Router
	Conn    *sql.DB
}

func (servS *ServerStats) handleRequests() {

	servS.routeur.HandleFunc("/", servS.homePage)
	servS.routeur.HandleFunc("/stats", servS.pageStats)
	servS.routeur.HandleFunc("/show-stats", servS.GUIStatsMaladie)
	servS.routeur.HandleFunc("/age-stats", servS.XMLStatsMaladieByAge)

	log.Fatal(http.ListenAndServe(":10000", servS.routeur)) //HTTP
	//log.Fatal(http.ListenAndServeTLS(":10000","localhost.crt", "localhost.key", servS.routeur)) //HTTPS -> nécessite un certificat SSH reconnu (marche mais affiche un message dans le navigateur
}

func (servS *ServerStats) homePage(w http.ResponseWriter, _ *http.Request) {
	_, err := fmt.Fprint(w, "Page des stats !")
	CheckErr(err, w)
	fmt.Println("Endpoint Hit: page des stats")
}

func (servS *ServerStats) Run() {
	servS.routeur = mux.NewRouter().StrictSlash(true)
	servS.handleRequests()
}

////////////////////////SERVICE DE STATS/////////////////////////
func (servS *ServerStats) pageStats(w http.ResponseWriter, _ *http.Request) {

	////////////////REQUETE///////////////
	// récupération du nombre de contamination peu importe la maladie
	row, err := servS.Conn.Query("select count(*) as total from contaminer")
	CheckErr(err, w)

	defer row.Close()

	var total string
	var totalI int
	if !row.Next() {
		_, err := fmt.Fprint(w, "Erreur : pas de donnée")
		CheckErr(err, w)
		return
	}

	err = row.Scan(&total)

	totalI, err = strconv.Atoi(total)
	CheckErr(err, w)

	if totalI == 0 {
		_, err := fmt.Fprint(w, "Aucune contamination enregistrée")
		CheckErr(err, w)
		return
	}

	//Récupération du nombre de contamination de chaque maladie
	rows, err := servS.Conn.Query("select count(*) as total, nomMaladie from contaminer join maladie using (idMaladie) group by (idMaladie)")
	CheckErr(err, w)

	defer rows.Close()

	servS.XMLStatsMaladie(rows, w, totalI)
}

func (servS *ServerStats) XMLStatsMaladie(rows *sql.Rows, w http.ResponseWriter, totalMaladie int) {

	var tmp, pourcentage float32
	var nomMaladie, pourcentag, totalCas, xmlContent string
	var check = true

	xmlContent += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
	xmlContent += "<stats>\n"

	for rows.Next() {
		check = false
		err := rows.Scan(&tmp, &nomMaladie)
		CheckErr(err, w)

		totalCas = strconv.Itoa(int(tmp))

		pourcentage = tmp / float32(totalMaladie) * 100
		pourcentag = strconv.Itoa(int(pourcentage))

		xmlContent += "\t<maladie>\n"
		xmlContent += "\t\t<nom>" + nomMaladie + "</nom>\n"
		xmlContent += "\t\t<nbCas>" + totalCas + "</nbCas>\n"
		xmlContent += "\t\t<frequence>" + pourcentag + "</frequence>\n"
		xmlContent += "\t</maladie>\n"
	}
	xmlContent += "</stats>\n"

	if check {
		w.WriteHeader(http.StatusNoContent)
	} else {
		_, err := fmt.Fprint(w, xmlContent)
		CheckErr(err, w)
	}
}

//Bonus
func (servS *ServerStats) GUIStatsMaladie(w http.ResponseWriter, _ *http.Request) {
	////////////////REQUETE///////////////
	// récupération du nombre de contamination peu importe la maladie
	row, err := servS.Conn.Query("select count(*) as total from contaminer")
	CheckErr(err, w)
	defer row.Close()

	var total string
	var totalMalade int
	if !row.Next() {
		_, err := fmt.Fprint(w, "Erreur : pas de donnée")
		CheckErr(err, w)
		return
	}

	err = row.Scan(&total)
	CheckErr(err, w)

	totalMalade, err = strconv.Atoi(total)
	CheckErr(err, w)

	if totalMalade == 0 {
		_, err := fmt.Fprint(w, "Aucune contamination enregistrée")
		CheckErr(err, w)
		return
	}

	//Récupération du nombre de contamination de chaque maladie
	rows, err := servS.Conn.Query("select count(*) as total, nomMaladie from contaminer join maladie using (idMaladie) group by (idMaladie)")
	CheckErr(err, w)
	defer rows.Close()

	//////////////FICHIER HTML DE BASE///////////////
	modele, err := ioutil.ReadFile("./interface/index.html")
	CheckErr(err, w)

	//envoi de l'en tête
	_, err = fmt.Fprint(w, string(modele))
	CheckErr(err, w)

	for rows.Next() {
		var tmp, pourcentage float32
		var nomMaladie, statMaladie, pourcentag string

		err := rows.Scan(&tmp, &nomMaladie)
		CheckErr(err, w)

		pourcentage = tmp / float32(totalMalade) * 100
		pourcentag = strconv.Itoa(int(pourcentage))

		statMaladie = "<dd class=\"percentage percentage-" + pourcentag + "\"><span class=\"text\">" + nomMaladie + " : " + pourcentag + "%" + "</span></dd>"

		//Envoi des données de la maladie courante
		_, err = fmt.Fprint(w, statMaladie)
		CheckErr(err, w)
	}

	//Fin de la page
	_, err = fmt.Fprint(w, "    </dl>\n</body>\n</html>\n")
	CheckErr(err, w)
}

func (servS *ServerStats) XMLStatsMaladieByAge(w http.ResponseWriter, _ *http.Request) {

	var totalMalade int
	var rows *sql.Rows
	var err error

	reqCompteurMaladie := "select nomMaladie, count(*) as total, ROUND(DATEDIFF(CURRENT_DATE, dateNaissance)/365, 0) as age" +
		" from contaminer" +
		" join maladie using (idMaladie)" +
		" join patient using (numSecu)" +
		" where ROUND(DATEDIFF(CURRENT_DATE, dateNaissance)/365, 0) between ? and ?" +
		" group by (idMaladie)"

	reqTotalMalade := "select count(*) as total from contaminer join patient using (numSecu) where ROUND(DATEDIFF(CURRENT_DATE, dateNaissance)/365, 0) between ? and ?"

	var xmlAges [6]string

	var rowS [6]*sql.Rows
	for i := 0; i < 6; i++ {
		rows, err = servS.Conn.Query(reqTotalMalade, i*20, (i+1)*20)
		if !CheckErr(err, w) {
			return
		}

		if rows.Next() {
			err = rows.Scan(&totalMalade)
			CheckErr(err, w)
		} else {
			fmt.Println("Pas de contenu")
			w.WriteHeader(http.StatusNoContent)
			return
		}
		rows.Close()

		rowS[i], err = servS.Conn.Query(reqCompteurMaladie, i*20, (i+1)*20)
		if !CheckErr(err, w) {
			w.WriteHeader(http.StatusNoContent)
			return
		}

		borninf := strconv.Itoa(i * 20)
		bornsup := strconv.Itoa((i+1)*20 - 1)

		xmlAges[i] = "\t<tranche>\n\t\t<ages>" + borninf + "-" + bornsup + "</ages>\n"

		var nomMaladie, tempTotal, tempAge string

		for rowS[i].Next() {
			err = rowS[i].Scan(&nomMaladie, &tempTotal, &tempAge)
			CheckErr(err, w)

			total, err := strconv.Atoi(tempTotal)
			CheckErr(err, w)
			frequence := float32(total) / float32(totalMalade) * 100
			strFrequence := strconv.Itoa(int(frequence))

			xmlAges[i] += "\t\t<maladie>" + nomMaladie + "</maladie>\n"
			xmlAges[i] += "\t\t<frequence>" + strFrequence + "%</frequence>\n"
		}

		xmlAges[i] += "\t</tranche>\n"
	}

	//Envoi des données et fermeture des requètes

	_, err = fmt.Fprint(w, "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n")
	CheckErr(err, w)
	_, err = fmt.Fprint(w, "<tranches>\n")
	CheckErr(err, w)

	for i := 0; i < 6; i++ {
		_, err = fmt.Fprint(w, xmlAges[i])
		CheckErr(err, w)
	}
	_, err = fmt.Fprint(w, "</tranches>\n")
	CheckErr(err, w)
}
